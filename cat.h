///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.h
/// @version 1.0
///
/// Exports data about cats
///
/// @author Alexander Sidelev <asidelev@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   01_30_2021 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "animals.h"
#include <string.h>
#include <stdbool.h> 
///declaration of cat breeds
enum CatBreeds {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
//Return a string for cat breed

char* catBreedsName (enum CatBreeds breeds) ; 

/// declaration of a struct Cat
struct Cat {
   char     name[30];
   enum     Gender gender;
   enum     CatBreeds breed;
   bool     isFixed;
   float    weight;
   enum     Color collar1_color;
   enum     Color collar2_color;
   long     license;

};

/// add Alice the cat to the catabase

void addAliceTheCat(int i) ;

/// Print a cat from the database
void printCat(int i) ;

