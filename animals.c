///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.1
///
/// Helper functions that apply to animals great and small
///
/// @author Alexnader Sidelev <asidelev@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   01_30_2021 
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"
/// Decode the enum Gender to stings for printf()
char* genderName (enum Gender gender) {
   switch(gender){
      case MALE:
         return "Male";
      case FEMALE: 
         return "Female";
      default:
         return NULL;   
   }
};

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {
   switch(color){
      case BLACK:
         return "Black";
      case RED:
         return "Red";
      case WHITE:
         return "White";
      case BLUE:
         return "Blue";
      case GREEN:
         return "Green";
      case PINK:
            return "Pink";
      default:
         return NULL;
   }
};

