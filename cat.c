///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.1
///
/// Implements a simple database that manages cats
///
/// @author Alexander Sidelev <asidelev@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   01_30_2021
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "cat.h"
#include "animals.h"

// Declaration of an array of struct Cat, bound by MAX_SPECIES

struct Cat catDB[MAX_SPECIES];

/// Decode Cat Breeds for printf()
char* catBreedsName (enum CatBreeds breed) {
   switch(breed){
      case MAIN_COON:
         return "Main Coon";
      case MANX:
         return "Manx";
      case SHORTHAIR:
         return "Shorthair";
      case PERSIAN:
         return "Persian";
      case SPHYNX:
         return "Sphynx";
      default: 
         return NULL;
   }
};

/// Addion of Alice to the Cat catabase at position i.
void addAliceTheCat(int i) {
   strcpy(catDB[i].name, "Alice");
   catDB[i].gender            =  FEMALE;
   catDB[i].breed             =  MAIN_COON;
   catDB[i].isFixed           =  true;
   catDB[i].weight            =  12.34;
   catDB[i].collar1_color     =  BLACK;
   catDB[i].collar2_color     =  RED;
   catDB[i].license           =  12345;
};
/// Print a cat from the database

void printCat(int i) {
   printf("Cat Name = [%s]\n", (catDB[i].name));
   printf("    gender = [%s]\n", genderName(catDB[i].gender));
   printf("    Cat Breed = [%s]\n", catBreedsName(catDB[i].breed));
   printf("    isFixed = [%s]\n", (catDB[i].isFixed)? "yes":"no"); //a trick to print bool vals as a char
   printf("    weight = [%f]\n", (catDB[i].weight));
   printf ("    collar color 1 = [%s]\n", colorName(catDB[i].collar1_color));
   printf ("    collar color 2 = [%s]\n",colorName(catDB[i].collar2_color));
   printf ("    license = [%ld]\n", (catDB[i].license));
};

