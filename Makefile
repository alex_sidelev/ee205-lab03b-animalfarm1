###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Alexander Sidelev <asidelev@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   01_30_2021
###############################################################################
CC     = gcc
CFLAGS = -g -Wall 

TARGET = animalfarm 

all: $(TARGET)

animalfarm.o: main.c main.h cat.c cat.h animals.c animals.h 


animalfarm: main.o cat.o animals.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o cat.o animals.o


clean:
	rm -f *.o $(TARGET) 
